@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><center>Data Antrian</center></h1>
            
            <a href="/back" type="button" class="btn btn-warning">Ambil Nomor Antrian</a>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="container">
        <div class="row">
            <table class="table">
                <thead>
                    <br><br><br>
                  <tr>
                    <th scope="col">Nomor Antrian</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>

                @foreach ($data as $row)
                <tr>
                <td>{{ $row->id }}</td>
                    <td>
                        <a href=""  class="btn btn-warning">Panggil</a>
                        <a href="/ada/{{ $row->id }}" class="btn btn-info">Ada</a>
                        <a href="/hapus/{{ $row->id }}" class="btn btn-danger">Tidak Ada</a>
                    </td>
                  </tr>
                @endforeach

                  
                </tbody>
              </table>
            </div>
            </div>
        </div>
</div>
@endsection