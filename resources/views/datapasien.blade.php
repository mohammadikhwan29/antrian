@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Pasien</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Pasien</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <div class="container">
        <a href="/tambahpasien" type="button" class="btn btn-success">Tambah Data Pasien</a>
        <div class="row">
          @if ($message = Session::get('success'))
          <div class="alert alert-primary" role="alert">
             {{ $message }}
          </div>
          @endif
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Jenis Kelamin</th>
                    <th scope="col">Umur</th>
                    <th scope="col">No telepon</th>
                    <th scope="col">Poli</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>

                @php
                $no = 1; 
                @endphp

                @foreach ($data as $row)
                <tr>
                    <th scope="row">{{ $no++ }}</th>
                    <td>{{ $row->nama }}</td>
                    <td>{{ $row->jeniskelamin }}</td>
                    <td>{{ $row->umur }} Tahun</td>
                    <td>0{{ $row->notelpon }}</td>
                    <td>{{ $row->poli }}</td>
                    <td>{{ $row->alamat }}</td>
                    <td>
                        <a href="/tampildata/{{ $row->id }}"  class="btn btn-info">Edit</a>
                        <a href="/delete/{{ $row->id }}" class="btn btn-danger">Delete</a>
                    </td>
                  </tr>
                @endforeach

                  
                </tbody>
              </table>
              <a href="/kembali" type="button" class="btn btn-warning">Kembali Ke Antrian</a>
        </div>
      </div>
</div>


@endsection