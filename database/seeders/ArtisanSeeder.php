<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArtisanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('antrians')->insert([
            'nama' => 'ary',
            'umur' => '21',
            'notelpon' => '082169872',
            'alamat' => 'pare',
        ]);
    }
}
