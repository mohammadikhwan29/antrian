<?php

namespace App\Http\Controllers;

use App\Models\Antrian;
use App\Models\Pasien;
use Illuminate\Http\Request;

class AntrianController extends Controller
{
    public function index(){

        $data = Antrian::all();
        return view('datapasien', compact('data'));
    }

    public function tambahpasien(){
        return view('tambahdata');
    }

    public function insertdata(Request $request){
        Antrian::create($request->all());
        return redirect()->route('pasien')->with('success','Data Berhasil Ditambahkan');
    }

    public function tampildata($id){
        $data = Antrian::find($id);
        return view('tampildata', compact('data'));
    }

    public function updatedata(Request $request, $id){
        $data = Antrian::find($id);
        $data->update($request->all());
        return redirect()->route('pasien')->with('success','Data Berhasil Diupdate');
    }

    public function delete($id){
        $data = Antrian::find($id);
        $data->delete();
        return redirect()->route('pasien')->with('success','Data Berhasil Dihapus');
    }
}
